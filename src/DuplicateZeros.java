import java.util.Scanner;

public class DuplicateZeros {
    public static int arr[];
    
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        System.out.println("Input: [1, 0, 2, 3, 0, 4, 5, 0]\n");
        int arr[] = {1, 0, 2, 3, 0, 4, 5, 0};
        DuplicateZeros(arr);
    }

    private static void DuplicateZeros(int[] arr) {
        int arrSize = arr.length;
        int arrCopy[] = new int[arrSize*2];
        int j = 0;

        for (int i=0; i<arrSize; i++){
            arrCopy[j++] = arr[i];
            if(arr[i] == 0){
                arrCopy[j++] = 0;
            }
        }

        System.out.println("Output: ");
        for (int i=0; i<arrSize; i++){
            System.out.print(arrCopy[i] + " ");
        }
    }

    
}
